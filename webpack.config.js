const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebPackPlugin = require("html-webpack-plugin");
const path = require('path');
const Dotenv = require('dotenv-webpack');

module.exports = {
    entry: "./src/index.js",
    output: {
        filename: 'bundled.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [{
            enforce: 'pre',
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'eslint-loader',
            options: {
                configFile: path.resolve(__dirname, '.eslintrc'),
                emitError: false,
                emitWarning: true
            }
        }, {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader'
            }
        }, {
            test: /\.scss$/,
            use: [
                'style-loader',
                'css-loader',
                {
                    loader: 'sass-loader',
                    options: {
                        data: `
                            @import "variables";
                        `,
                        includePaths: [
                            path.resolve(__dirname, 'assets/scss')
                        ]
                    }
                }
            ]
        }]
    },
    resolve: {
        // File extensions. Add others and needed (e.g. scss, json)
        extensions: ['.js', '.jsx'],
        modules: ['node_modules'],
        // Aliases help with shortening relative paths
        // 'Components/button' === '../../../components/button'
        alias: {
            Components: path.resolve(__dirname, 'src', 'components'),
            Utils: path.resolve(__dirname, 'src', 'utils'),
        },
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new Dotenv(),
        new HtmlWebPackPlugin({
            template: './src/index.html',
        })
    ],
    devServer: {
        // Proxying call to BreweryDB due to CORS issues
        proxy: {
            '/api': {
                target: 'https://sandbox-api.brewerydb.com/v2/',
                changeOrigin: true,
                pathRewrite: {'^/api' : ''}
            }
        }
    }
};
