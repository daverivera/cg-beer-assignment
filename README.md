# A take on CG Beer Assignment

**Important:** Before running the application with `yarn start` you must add `BreweryDB API Key` to the `.env` file provided in the email.

## Architecture

The entire project's architecture is based on [Vertical Architecture](https://jimmybogard.com/vertical-slice-architecture/) in order to allocate components and their children in hierarchy to not only [show intent](https://youtu.be/Nsjsiz2A9mg?t=419) about what the application is about but to allow other developers traverse through the application. By using folders such as `components` and `containers` won't help understand the flow and relationship between the components which makes it more difficult to keep track of data from top-down and bottom-up flows.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.

### `yarn test:coverage`

Launches the test runner and creates the coverage of the application.
Open the `coverage/lcov-report/index.html` file in a browser to check the coverage, although coverage is returned on the console, the browser easies browsing the coverage per file.

### `yarn run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.
The build is minified and the filenames include the hashes.

#### Important
Due to the limitation of BreweryDB and the inability to provide CORS support you have to deploy this in a server where a proxy to `https://sandbox-api.brewerydb.com/v2/` and expose it as `/api`. However, this is supported with `yarn start`

