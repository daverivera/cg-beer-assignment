import { MAXIMUM_NUMBER_FAVORITE_BEERS } from './constants';

const isBeerAlreadyAdded = beerId => ({ id: favoriteId }) => favoriteId === beerId;

const addFavoriteBeer = (favorites, { id, name, image }) => {
    const isBeerAlreadyFavorite = !!favorites.find(isBeerAlreadyAdded(id));
    const isListInMaximumCapacity = favorites.length === MAXIMUM_NUMBER_FAVORITE_BEERS;
    const isListRemainingUntouched = isListInMaximumCapacity && !isBeerAlreadyFavorite;

    if (isListRemainingUntouched) {
        return favorites;
    }

    return isBeerAlreadyFavorite // Then filter out the favorite beers
        ? favorites.filter(favoriteBeer => !isBeerAlreadyAdded(id)(favoriteBeer))
        : favorites.concat({
            id,
            image,
            isFavorite: true,
            name
        });
};

const processBeers = (beers, favorites) =>
    beers
        .map(({
            id,
            image,
            labels = {},
            name
        }) => ({
            id,
            image: labels.icon || image,
            isFavorite: !!favorites.find(isBeerAlreadyAdded(id)),
            name
        }));

const processBeerDetails = (beer, favorites) => ({
    description: beer.description,
    id: beer.id,
    isFavorite: !!favorites.find(isBeerAlreadyAdded(beer.id)),
    isOrganic: beer.isOrganic === 'Y',
    labels: beer.labels || {},
    name: beer.name,
    style: {
        name: beer.style.name,
        abv: `${beer.style.abvMin} - ${beer.style.abvMax}`,
        ibu: `${beer.style.ibuMin} - ${beer.style.ibuMax}`,
        srm: `${beer.style.srmMin} - ${beer.style.srmMax}`,
        finalGravity: `${beer.style.fgMin} - ${beer.style.fgMax}`
    }
});

const updateBeerDetailsFavoritism = (beerDetails, favoriteBeerId) => {
    if (!beerDetails) {
        return undefined;
    }

    const isBeerAlreadyFavorite = beerDetails.isFavorite;
    const shouldToggleFavorite = beerDetails.id === favoriteBeerId;

    return shouldToggleFavorite
        ? Object.assign({}, beerDetails, { isFavorite: !isBeerAlreadyFavorite })
        : beerDetails;
};

export default {
    addFavoriteBeer,
    processBeerDetails,
    processBeers,
    updateBeerDetailsFavoritism
};
