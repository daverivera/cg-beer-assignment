import axios from 'axios';

import BeerHelper from './beer-helper';
import { API_URLS, BREWERY_DB_API_KEY, MAX_FETCHED_BEERS_NUMBER } from './constants';

const getRandomBeerList = favorites =>
    axios.get(API_URLS.randomBeers, {
        params: {
            key: BREWERY_DB_API_KEY,
            order: 'random',
            randomCount: MAX_FETCHED_BEERS_NUMBER
        }
    })
        .then(({ data }) => data.data)
        .then(beers => BeerHelper.processBeers(beers, favorites));

const getBeerDetails = (beerId, favorites) =>
    axios.get(`${API_URLS.beerDetails}/${beerId}`, {
        params: {
            key: BREWERY_DB_API_KEY
        }
    })
        .then(({ data }) => data.data)
        .then(beer => BeerHelper.processBeerDetails(beer, favorites));

export default {
    getBeerDetails,
    getRandomBeerList
};
