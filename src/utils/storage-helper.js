import { SESSION_STORAGE_KEYS } from './constants';

const retrieveBeerFavoritesList = () => {
    const favorites = JSON.parse(sessionStorage.getItem(SESSION_STORAGE_KEYS.favoriteBeers));
    return favorites || [];
};

const storeBeerFavoritesList = favorites => sessionStorage.setItem(
    SESSION_STORAGE_KEYS.favoriteBeers,
    JSON.stringify(favorites)
);

export default {
    favorites: {
        get: retrieveBeerFavoritesList,
        set: storeBeerFavoritesList
    }
};
