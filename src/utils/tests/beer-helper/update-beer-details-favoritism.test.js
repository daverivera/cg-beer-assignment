import BeerHelper from '../../beer-helper';

describe('BeerHelper', () => {
    describe('updateBeerDetailsFavoritism', () => {
        describe('when beer details is not defined', () => {
            it('should return undefied', () => {
                expect(BeerHelper.updateBeerDetailsFavoritism(undefined, 0))
                    .toEqual(undefined);
            });
        });

        describe('when beer details is not the selected favorite', () => {
            it('should return the same object untouched', () => {
                const beerDetails = {
                    id: 1,
                    isFavorite: false
                };
                const favoriteId = 2;
                expect(BeerHelper.updateBeerDetailsFavoritism(beerDetails, favoriteId))
                    .toEqual({
                        id: 1,
                        isFavorite: false
                    });
            });
        });

        describe('when beer details is the favorite', () => {
            describe('and the beer details is not marked as favorite', () => {
                it('should toggle the beer as favorite to true', () => {
                    const beerDetails = {
                        id: 1,
                        isFavorite: false
                    };
                    const favoriteId = 1;
                    expect(BeerHelper.updateBeerDetailsFavoritism(beerDetails, favoriteId))
                        .toEqual({
                            id: 1,
                            isFavorite: true
                        });
                });
            });

            describe('and the beer details is already marked as favorite', () => {
                it('should toggle the beer as favorite to false', () => {
                    const beerDetails = {
                        id: 1,
                        isFavorite: true
                    };
                    const favoriteId = 1;
                    expect(BeerHelper.updateBeerDetailsFavoritism(beerDetails, favoriteId))
                        .toEqual({
                            id: 1,
                            isFavorite: false
                        });
                });
            });
        });
    });
});
