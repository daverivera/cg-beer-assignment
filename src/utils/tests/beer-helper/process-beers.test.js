import BeerHelper from '../../beer-helper';

describe('BeerHelper', () => {
    describe('processBeers', () => {
        describe('when a list of beers is passed', () => {
            const BEER_LIST = [{
                id: 'TpiDMC',
                name: 'Brown Shugga',
                description: 'How Come you Taste So Good?? Boatloads of Pure Brown Sugar in Each Batch, That’s How!',
                labels: {
                    icon: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-icon.png'
                }
            }, {
                id: 'zd3xxc',
                name: 'Centennial Hop Mountain Marzen'
            }];

            describe('and the list of favorites is empty', () => {
                it('should return a new list of favorites with isFavorite as `false` and without unnecessary properties', () => {
                    const emptyFavoriteList = [];

                    expect(BeerHelper.processBeers([...BEER_LIST], emptyFavoriteList))
                        .toEqual([{
                            id: 'TpiDMC',
                            name: 'Brown Shugga',
                            isFavorite: false,
                            image: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-icon.png'
                        }, {
                            id: 'zd3xxc',
                            name: 'Centennial Hop Mountain Marzen',
                            isFavorite: false,
                            image: undefined
                        }]);
                });
            });

            describe('and the list of favorites is not empty ', () => {
                it('should return a new list of beers with isFavorite as `true` for the selected favorites, false for the non selected favorites and without unnecessary properties per Beer', () => {
                    const selectedFavoriteList = [{
                        id: 'TpiDMC',
                        image: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-icon.png',
                        isFavorite: true,
                        name: 'Brown Shugga'
                    }];

                    expect(BeerHelper.processBeers([...BEER_LIST], selectedFavoriteList))
                        .toEqual([{
                            id: 'TpiDMC',
                            name: 'Brown Shugga',
                            isFavorite: true,
                            image: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-icon.png'
                        }, {
                            id: 'zd3xxc',
                            name: 'Centennial Hop Mountain Marzen',
                            isFavorite: false,
                            image: undefined
                        }]);
                });
            });
        });

        describe('when the list of beers is already process', () => {
            describe('and the list of favorites is not empty', () => {
                it('should preserve the icon image and update the favorite beer', () => {
                    const selectedFavoriteList = [{ id: 'zd3xxc' }];
                    const beerList = [{
                        id: 'TpiDMC',
                        name: 'Brown Shugga',
                        isFavorite: false,
                        image: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-icon.png'
                    }, {
                        id: 'zd3xxc',
                        name: 'Centennial Hop Mountain Marzen',
                        isFavorite: false,
                        image: 'https://brewerydb-images.s3.amazonaws.com/beer/zd3xxc/upload_bjZMsj-icon.png'
                    }];

                    expect(BeerHelper.processBeers(beerList, selectedFavoriteList))
                        .toEqual([{
                            id: 'TpiDMC',
                            name: 'Brown Shugga',
                            isFavorite: false,
                            image: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-icon.png'
                        }, {
                            id: 'zd3xxc',
                            name: 'Centennial Hop Mountain Marzen',
                            isFavorite: true,
                            image: 'https://brewerydb-images.s3.amazonaws.com/beer/zd3xxc/upload_bjZMsj-icon.png'
                        }]);
                });
            });

            describe('and the list of favorites is empty', () => {
                it('should preserve the icon image and update the favorite beer', () => {
                    const emptyFavoriteList = [];
                    const beerList = [{
                        id: 'TpiDMC',
                        name: 'Brown Shugga',
                        isFavorite: true,
                        image: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-icon.png'
                    }, {
                        id: 'zd3xxc',
                        name: 'Centennial Hop Mountain Marzen',
                        isFavorite: false,
                        image: 'https://brewerydb-images.s3.amazonaws.com/beer/zd3xxc/upload_bjZMsj-icon.png'
                    }];

                    expect(BeerHelper.processBeers(beerList, emptyFavoriteList))
                        .toEqual([{
                            id: 'TpiDMC',
                            name: 'Brown Shugga',
                            isFavorite: false,
                            image: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-icon.png'
                        }, {
                            id: 'zd3xxc',
                            name: 'Centennial Hop Mountain Marzen',
                            isFavorite: false,
                            image: 'https://brewerydb-images.s3.amazonaws.com/beer/zd3xxc/upload_bjZMsj-icon.png'
                        }]);
                });
            });
        });
    });
});
