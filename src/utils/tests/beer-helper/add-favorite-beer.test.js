import BeerHelper from '../../beer-helper';

describe('BeerHelper', () => {
    describe('addFavoriteBeer', () => {
        describe('when adding a favorite Beer to the list', () => {
            describe('and the favorites list is empty', () => {
                it('should append the Beer object', () => {
                    const emptyFavoriteList = [];

                    const newFavoriteBeer = {
                        id: 'TpiDMC',
                        name: 'Brown Shugga',
                        image: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-icon.png'
                    };

                    expect(BeerHelper.addFavoriteBeer(emptyFavoriteList, newFavoriteBeer))
                        .toEqual([{
                            id: 'TpiDMC',
                            name: 'Brown Shugga',
                            isFavorite: true,
                            image: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-icon.png'
                        }]);
                });
            });

            describe('and the favorites list is not empty', () => {
                describe('and the new favorite beer is not in the list', () => {
                    it('should append the beer at the end', () => {
                        const nonRepeatedFavoriteBeersList = [{
                            id: 'TpiDMC',
                            name: 'Brown Shugga',
                            isFavorite: true,
                            image: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-icon.png'
                        }];

                        const newFavoriteBeer = {
                            id: 'zd3xxc',
                            name: 'Centennial Hop Mountain Marzen',
                            image: undefined
                        };

                        expect(BeerHelper.addFavoriteBeer(nonRepeatedFavoriteBeersList, newFavoriteBeer))
                            .toEqual([{
                                id: 'TpiDMC',
                                name: 'Brown Shugga',
                                isFavorite: true,
                                image: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-icon.png'
                            }, {
                                id: 'zd3xxc',
                                name: 'Centennial Hop Mountain Marzen',
                                isFavorite: true,
                                image: undefined
                            }]);
                    });
                });

                describe('and the new favorite beer already exists in the list', () => {
                    it('should remove the beer from the favorites list', () => {
                        const nonRepeatedFavoriteBeersList = [{
                            id: 'TpiDMC',
                            name: 'Brown Shugga',
                            isFavorite: true,
                            image: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-icon.png'
                        }, {
                            id: 'zd3xxc',
                            name: 'Centennial Hop Mountain Marzen',
                            isFavorite: true,
                            image: undefined
                        }];

                        const newFavoriteBeer = {
                            id: 'zd3xxc',
                            name: 'Centennial Hop Mountain Marzen',
                            image: undefined
                        };

                        expect(BeerHelper.addFavoriteBeer(nonRepeatedFavoriteBeersList, newFavoriteBeer))
                            .toEqual([{
                                id: 'TpiDMC',
                                name: 'Brown Shugga',
                                isFavorite: true,
                                image: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-icon.png'
                            }]);
                    });
                });

                describe.only('and the list hast already reached the maximum number of favorites, which is 10', () => {
                    describe('and the beer is in the not on the favorites list', () => {
                        it('should maintain the list without alterations and return it', () => {
                            const fullFavoritesList = [
                                { id: 0 },
                                { id: 1 },
                                { id: 2 },
                                { id: 3 },
                                { id: 4 },
                                { id: 5 },
                                { id: 6 },
                                { id: 7 },
                                { id: 8 },
                                { id: 9 }
                            ];

                            const newFavoriteBeer = {
                                id: 'TpiDMC',
                                name: 'Brown Shugga',
                                image: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-icon.png'
                            };

                            expect(BeerHelper.addFavoriteBeer(fullFavoritesList, newFavoriteBeer))
                                .toEqual([
                                    { id: 0 },
                                    { id: 1 },
                                    { id: 2 },
                                    { id: 3 },
                                    { id: 4 },
                                    { id: 5 },
                                    { id: 6 },
                                    { id: 7 },
                                    { id: 8 },
                                    { id: 9 }
                                ]);
                        });
                    });

                    describe('and the beer is in the favorites list', () => {
                        it('should remove the beer from the list', () => {
                            const fullFavoritesList = [
                                { id: 0 },
                                { id: 1 },
                                { id: 2 },
                                { id: 3 },
                                { id: 4 },
                                { id: 5 },
                                { id: 6 },
                                { id: 7 },
                                { id: 8 },
                                { id: 9 }
                            ];

                            const newFavoriteBeer = { id: 5 };

                            expect(BeerHelper.addFavoriteBeer(fullFavoritesList, newFavoriteBeer))
                                .toEqual([
                                    { id: 0 },
                                    { id: 1 },
                                    { id: 2 },
                                    { id: 3 }, // No beer with id 5
                                    { id: 4 },
                                    { id: 6 },
                                    { id: 7 },
                                    { id: 8 },
                                    { id: 9 }
                                ]);
                        });
                    });
                });
            });
        });
    });
});
