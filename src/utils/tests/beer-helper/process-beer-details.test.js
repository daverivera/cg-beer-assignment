import BeerHelper from '../../beer-helper';

describe('BeerHelper', () => {
    describe('processBeerDetails', () => {
        describe('when a beers is passed', () => {
            describe('and the favorites list is empty', () => {
                describe('and has no images', () => {
                    it('should set image object as an empty object', () => {
                        const beer = {
                            description: 'How Come you Taste So Good?? Boatloads of Pure Brown Sugar in Each Batch, That’s How!',
                            id: 'TpiDMC',
                            isOrganic: 'N',
                            name: 'Brown Shugga',
                            style: {
                                name: 'American-Style India Pale Ale',
                                abvMax: '7.5',
                                abvMin: '6.3',
                                fgMax: '1.018',
                                fgMin: '1.012',
                                ibuMax: '70',
                                ibuMin: '50',
                                srmMax: '14',
                                srmMin: '6'
                            }
                        };

                        const emptyFavoriteList = [];

                        expect(BeerHelper.processBeerDetails(beer, emptyFavoriteList))
                            .toEqual({
                                description: 'How Come you Taste So Good?? Boatloads of Pure Brown Sugar in Each Batch, That’s How!',
                                id: 'TpiDMC',
                                isOrganic: false,
                                isFavorite: false,
                                name: 'Brown Shugga',
                                labels: {},
                                style: {
                                    name: 'American-Style India Pale Ale',
                                    abv: '6.3 - 7.5',
                                    ibu: '50 - 70',
                                    srm: '6 - 14',
                                    finalGravity: '1.012 - 1.018'
                                }
                            });
                    });
                });

                describe('and is not organic', () => {
                    it('should parse the beer data', () => {
                        const beer = {
                            description: 'How Come you Taste So Good?? Boatloads of Pure Brown Sugar in Each Batch, That’s How!',
                            id: 'TpiDMC',
                            isOrganic: 'N',
                            name: 'Brown Shugga',
                            labels: {
                                icon: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-icon.png',
                                medium: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-medium.png'
                            },
                            style: {
                                name: 'American-Style India Pale Ale',
                                abvMax: '7.5',
                                abvMin: '6.3',
                                fgMax: '1.018',
                                fgMin: '1.012',
                                ibuMax: '70',
                                ibuMin: '50',
                                srmMax: '14',
                                srmMin: '6'
                            }
                        };

                        const emptyFavoriteList = [];

                        expect(BeerHelper.processBeerDetails(beer, emptyFavoriteList))
                            .toEqual({
                                description: 'How Come you Taste So Good?? Boatloads of Pure Brown Sugar in Each Batch, That’s How!',
                                id: 'TpiDMC',
                                isOrganic: false,
                                isFavorite: false,
                                name: 'Brown Shugga',
                                labels: {
                                    icon: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-icon.png',
                                    medium: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-medium.png'
                                },
                                style: {
                                    name: 'American-Style India Pale Ale',
                                    abv: '6.3 - 7.5',
                                    ibu: '50 - 70',
                                    srm: '6 - 14',
                                    finalGravity: '1.012 - 1.018'
                                }
                            });
                    });
                });

                describe('and is organic', () => {
                    it('should parse the beer data', () => {
                        const beer = {
                            description: 'How Come you Taste So Good?? Boatloads of Pure Brown Sugar in Each Batch, That’s How!',
                            id: 'TpiDMC',
                            isOrganic: 'Y',
                            name: 'Brown Shugga',
                            labels: {
                                icon: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-icon.png',
                                medium: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-medium.png'
                            },
                            style: {
                                name: 'American-Style India Pale Ale',
                                abvMax: '7.5',
                                abvMin: '6.3',
                                fgMax: '1.018',
                                fgMin: '1.012',
                                ibuMax: '70',
                                ibuMin: '50',
                                srmMax: '14',
                                srmMin: '6'
                            }
                        };

                        const emptyFavoriteList = [];

                        expect(BeerHelper.processBeerDetails(beer, emptyFavoriteList))
                            .toEqual({
                                description: 'How Come you Taste So Good?? Boatloads of Pure Brown Sugar in Each Batch, That’s How!',
                                id: 'TpiDMC',
                                isOrganic: true,
                                isFavorite: false,
                                name: 'Brown Shugga',
                                labels: {
                                    icon: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-icon.png',
                                    medium: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-medium.png'
                                },
                                style: {
                                    name: 'American-Style India Pale Ale',
                                    abv: '6.3 - 7.5',
                                    ibu: '50 - 70',
                                    srm: '6 - 14',
                                    finalGravity: '1.012 - 1.018'
                                }
                            });
                    });
                });
            });

            describe('and the beer is already a favorite', () => {
                it('should parse the beer details and set "isFavorite" to true', () => {
                    const beer = {
                        description: 'How Come you Taste So Good?? Boatloads of Pure Brown Sugar in Each Batch, That’s How!',
                        id: 'TpiDMC',
                        isOrganic: 'N',
                        name: 'Brown Shugga',
                        labels: {
                            icon: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-icon.png',
                            medium: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-medium.png'
                        },
                        style: {
                            name: 'American-Style India Pale Ale',
                            abvMax: '7.5',
                            abvMin: '6.3',
                            fgMax: '1.018',
                            fgMin: '1.012',
                            ibuMax: '70',
                            ibuMin: '50',
                            srmMax: '14',
                            srmMin: '6'
                        }
                    };

                    const favoritesList = [{ id: 'TpiDMC' }];

                    expect(BeerHelper.processBeerDetails(beer, favoritesList))
                        .toEqual({
                            description: 'How Come you Taste So Good?? Boatloads of Pure Brown Sugar in Each Batch, That’s How!',
                            id: 'TpiDMC',
                            isOrganic: false,
                            isFavorite: true,
                            name: 'Brown Shugga',
                            labels: {
                                icon: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-icon.png',
                                medium: 'https://brewerydb-images.s3.amazonaws.com/beer/TpiDMC/upload_bjZMsj-medium.png'
                            },
                            style: {
                                name: 'American-Style India Pale Ale',
                                abv: '6.3 - 7.5',
                                ibu: '50 - 70',
                                srm: '6 - 14',
                                finalGravity: '1.012 - 1.018'
                            }
                        });
                });
            });
        });
    });
});
