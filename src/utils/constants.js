const BREWERY_SERVER = '/api';

// eslint-disable-next-line prefer-destructuring
export const BREWERY_DB_API_KEY = process.env.BREWERY_DB_API_KEY;

export const API_URLS = {
    beerDetails: `${BREWERY_SERVER}/beer`,
    randomBeers: `${BREWERY_SERVER}/beers`
};

export const MAXIMUM_NUMBER_FAVORITE_BEERS = 10;
export const MAX_FETCHED_BEERS_NUMBER = 10;

export const SESSION_STORAGE_KEYS = {
    favoriteBeers: 'favoriteBeers'
};
