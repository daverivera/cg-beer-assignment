import React, { Component } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';

import BeerApi from 'Utils/beer-api';
import BeerHelper from 'Utils/beer-helper';
import StorageHelper from 'Utils/storage-helper';

import Beers from './beers';
import BeerDetails from './beer-details';

class GCBeers extends Component {
    state = {
        beers: [],
        beerDetails: undefined,
        favorites: []
    }

    componentDidMount() {
        this.getRandomBeers();
    }

    getBeerDetails = (beerId) => {
        const { favorites } = this.state;

        BeerApi
            .getBeerDetails(beerId, favorites)
            .then((beerDetails) => {
                this.setState({ beerDetails });
            });
    }

    getRandomBeers = () => {
        const { favorites } = this.state;

        BeerApi
            .getRandomBeerList(favorites)
            .then((beers) => {
                this.setState({
                    beers,
                    favorites: StorageHelper.favorites.get()
                });
            });
    }

    onReturnToHome = () => {
        this.setState({ beerDetails: undefined });
    }

    updateFavorites = (newFavoriteBeer) => {
        const { beers, beerDetails, favorites } = this.state;
        const newFavoritesList = BeerHelper.addFavoriteBeer(favorites, newFavoriteBeer);
        const newBeerDetails = BeerHelper.updateBeerDetailsFavoritism(beerDetails, newFavoriteBeer.id);
        StorageHelper.favorites.set(newFavoritesList);

        this.setState({
            beers: BeerHelper.processBeers(beers, newFavoritesList),
            beerDetails: newBeerDetails,
            favorites: newFavoritesList
        });
    }

    render() {
        const { beers, beerDetails, favorites } = this.state;

        return (
            <HashRouter>
                <Switch>
                    <Route
                        exact
                        path="/"
                        render={() => (
                            <Beers
                                beers={beers}
                                favorites={favorites}
                                getRandomBeers={this.getRandomBeers}
                                updateFavorites={this.updateFavorites}
                            />
                        )}
                    />
                    <Route
                        path="/beer/:beerId"
                        render={({ match }) => (
                            <BeerDetails
                                beer={beerDetails}
                                beerId={match.params.beerId}
                                getBeerDetails={this.getBeerDetails}
                                onReturnToHome={this.onReturnToHome}
                                updateFavorites={this.updateFavorites}
                            />
                        )}
                    />
                </Switch>
            </HashRouter>
        );
    }
}

export default GCBeers;
