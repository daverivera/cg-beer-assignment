import React from 'react';
import PropTypes from 'prop-types';
import { TabsContainer, Tabs, Tab } from 'react-md';

import Button from 'Components/button';
import BeerList from 'Components/beer-list';

import './style.scss';

const Beers = ({
    beers,
    favorites,
    getRandomBeers,
    updateFavorites
}) => (
    <TabsContainer colored>
        <Tabs tabId="beer-tab">
            <Tab label="Random">
                <div className="beers__refresh">
                    <Button onClick={getRandomBeers}>
                        Refresh
                    </Button>
                </div>
                <BeerList
                    beers={beers}
                    updateFavorites={updateFavorites}
                />
            </Tab>
            <Tab label={`Favorites (${favorites.length})`}>
                <BeerList
                    beers={favorites}
                    updateFavorites={updateFavorites}
                />
            </Tab>
        </Tabs>
    </TabsContainer>
);

Beers.propTypes = {
    beers: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        image: PropTypes.string,
        isFavorite: PropTypes.bool.isRequired,
        name: PropTypes.string.isRequired
    })).isRequired,
    favorites: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        image: PropTypes.string,
        isFavorite: PropTypes.bool.isRequired,
        name: PropTypes.string.isRequired
    })).isRequired,
    getRandomBeers: PropTypes.func.isRequired,
    updateFavorites: PropTypes.func.isRequired
};

export default Beers;
