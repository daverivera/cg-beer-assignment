import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './style.scss';

class BeerStyle extends PureComponent {
    static propTypes = {
        style: PropTypes.shape({
            abv: PropTypes.string.isRequired,
            finalGravity: PropTypes.string.isRequired,
            ibu: PropTypes.string.isRequired,
            srm: PropTypes.string.isRequired
        }).isRequired
    }

    render() {
        const { style } = this.props;

        return (
            <div className="beer-style">
                <h2 className="beer-style__title">Style Characteristics</h2>

                <div className="beer-style__characteristics">
                    <div className="beer-style__attribute">
                        <span className="beer-style__attribute-name">ABV</span>
                        <span className="beer-style__atribute-value">{style.abv}</span>
                    </div>
                    <div className="beer-style__attribute">
                        <span className="beer-style__attribute-name">IBU</span>
                        <span className="beer-style__atribute-value">{style.ibu}</span>
                    </div>
                    <div className="beer-style__attribute">
                        <span className="beer-style__attribute-name">SRM</span>
                        <span className="beer-style__atribute-value">{style.srm}</span>
                    </div>
                    <div className="beer-style__attribute">
                        <span className="beer-style__attribute-name">Final Gravity</span>
                        <span className="beer-style__atribute-value">{style.finalGravity}</span>
                    </div>
                </div>
            </div>
        );
    }
}

export default BeerStyle;
