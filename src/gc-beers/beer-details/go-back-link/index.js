import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './style.scss';

const GoBackLink = ({ onClick }) => (
    <Link to="/" className="go-back-link" onClick={onClick}>
        <i className="material-icons go-back-link__arrow">
            keyboard_backspace
        </i>
        <span>Go back</span>
    </Link>
);

GoBackLink.propTypes = {
    onClick: PropTypes.func.isRequired
};

export default GoBackLink;
