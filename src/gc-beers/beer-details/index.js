import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Cell, Grid } from 'react-md';

import FavoriteStar from 'Components/favorite-star';

import BeerStyle from './beer-style';
import CertifiedOrganic from './certified-organic';
import GoBackLink from './go-back-link';

import './style.scss';

class BeerDetails extends Component {
    static propTypes = {
        beerId: PropTypes.string.isRequired,
        beer: PropTypes.shape({
            description: PropTypes.string.isRequired,
            id: PropTypes.string.isRequired,
            isFavorite: PropTypes.bool.isRequired,
            isOrganic: PropTypes.bool.isRequired,
            labels: PropTypes.shape({
                icon: PropTypes.string,
                medium: PropTypes.string
            }),
            name: PropTypes.string.isRequired,
            style: PropTypes.shape({
                abv: PropTypes.string.isRequired,
                finalGravity: PropTypes.string.isRequired,
                ibu: PropTypes.string.isRequired,
                name: PropTypes.string.isRequired,
                srm: PropTypes.string.isRequired
            }).isRequired
        }),
        getBeerDetails: PropTypes.func.isRequired,
        onReturnToHome: PropTypes.func.isRequired,
        updateFavorites: PropTypes.func.isRequired
    }

    static defaultProps = {
        beer: undefined
    }

    componentDidMount() {
        const { beerId, getBeerDetails } = this.props;
        getBeerDetails(beerId);
    }

    addBeerToFavorites = () => {
        const { beer, updateFavorites } = this.props;

        updateFavorites({
            id: beer.id,
            image: beer.labels.icon,
            name: beer.name
        });
    }

    render() {
        const { beer, onReturnToHome } = this.props;

        if (!beer) {
            return (
                <div className="beer-details--empty">
                    <h3>Loading beer...</h3>
                </div>
            );
        }

        return (
            <div className="beer-details">
                <div className="beer-details__header">
                    <GoBackLink onClick={onReturnToHome} />
                </div>
                <Grid>
                    <Cell size={9}>
                        <div className="beer-details__title">
                            <span className="beer-details__name">{beer.name}</span>
                            <CertifiedOrganic isOrganic={beer.isOrganic} />
                            <FavoriteStar isFavorite={beer.isFavorite} onClick={this.addBeerToFavorites} />
                        </div>

                        <div className="beer-details__description">
                            <b>Description</b>
                            <p>{beer.description}</p>
                        </div>

                        <div className="beer-details__style">
                            <div>
                                <BeerStyle style={beer.style} />
                            </div>
                            <div>
                                <b>Beer style</b>
                                <br />
                                <span>{beer.style.name}</span>
                            </div>
                        </div>
                    </Cell>
                    <Cell size={3} className="beer-details__image">
                        <img src={beer.labels.medium} alt="" />
                    </Cell>
                </Grid>
            </div>
        );
    }
}

export default BeerDetails;
