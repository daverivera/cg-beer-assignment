import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './style.scss';

class CertifiedOrganic extends PureComponent {
    static propTypes = {
        isOrganic: PropTypes.bool.isRequired
    }

    render() {
        const { isOrganic } = this.props;

        return (
            <div className={classNames(
                'certified-organic',
                {
                    'certified-organic--is-organic': isOrganic,
                    'certified-organic--not-organic': !isOrganic
                }
            )}
            >
                <span>Organic</span>
            </div>
        );
    }
}

export default CertifiedOrganic;
