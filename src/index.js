import React from 'react';
import ReactDOM from 'react-dom';

import GCBeers from './gc-beers';

import './web-fonts';
import './style.scss';

ReactDOM.render(
    <GCBeers />,
    document.getElementById('root')
);
