import React from 'react';
import PropTypes from 'prop-types';
import { Button as ReactMdButton } from 'react-md';

const Button = ({ children, onClick }) => <ReactMdButton primary flat swapTheming onClick={onClick}>{children}</ReactMdButton>;

Button.propTypes = {
    children: PropTypes.node.isRequired,
    onClick: PropTypes.func
};

Button.defaultProps = {
    onClick: () => {}
};

export default Button;
