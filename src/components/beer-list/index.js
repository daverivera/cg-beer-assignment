import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Cell, Grid } from 'react-md';

import BeerCard from './beer-card';

import './style.scss';

class BeerList extends PureComponent {
    static propTypes = {
        beers: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.string.isRequired,
            image: PropTypes.string,
            isFavorite: PropTypes.bool.isRequired,
            name: PropTypes.string.isRequired
        })).isRequired,
        updateFavorites: PropTypes.func.isRequired
    }

    renderBeers() {
        const { beers, updateFavorites } = this.props;

        return beers.map(beer => (
            <Cell size={6} key={beer.id}>
                <BeerCard {...beer} updateFavorites={updateFavorites} />
            </Cell>
        ));
    }

    render() {
        return (
            <Grid>
                {this.renderBeers()}
            </Grid>
        );
    }
}

export default BeerList;
