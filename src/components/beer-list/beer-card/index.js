import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Card } from 'react-md';
import { Link } from 'react-router-dom';

import FavoriteStar from 'Components/favorite-star';
import Button from 'Components/button';

import './style.scss';

class BeerCard extends PureComponent {
    static propTypes = {
        id: PropTypes.string.isRequired,
        image: PropTypes.string,
        isFavorite: PropTypes.bool.isRequired,
        name: PropTypes.string.isRequired,
        updateFavorites: PropTypes.func.isRequired
    }

    static defaultProps = {
        image: undefined
    }

    addBeerToFavorites = () => {
        const {
            id,
            image,
            name,
            updateFavorites
        } = this.props;

        updateFavorites({ id, image, name });
    }

    render() {
        const {
            id,
            image,
            isFavorite,
            name
        } = this.props;

        return (
            <Card className="beer-card">
                <div className="beer-card__icon">
                    <img src={image} alt="" />
                </div>
                <div className="beer-card__details">
                    <span>{name}</span>
                    <Link to={`/beer/${id}`}>
                        <Button>Details</Button>
                    </Link>
                </div>
                <div className="beer-card__favorite">
                    <FavoriteStar isFavorite={isFavorite} onClick={this.addBeerToFavorites} />
                </div>
            </Card>
        );
    }
}

export default BeerCard;
