import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import './style.scss';

const FavoriteStar = ({ isFavorite, onClick }) => {
    const starName = isFavorite ? 'star' : 'star_border';

    return (
        <i
            className={classNames(
                'material-icons',
                'favorite-star',
                {
                    'favorite-star--active': isFavorite
                }
            )}
            onClick={onClick}
        >
            {starName}
        </i>
    );
};

FavoriteStar.propTypes = {
    isFavorite: PropTypes.bool.isRequired,
    onClick: PropTypes.func.isRequired
};

export default FavoriteStar;
